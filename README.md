# ToDo app
---
ToDo app is a Todo API that can make a list of tasks and read it. Then it can also create a new user and login. This API using SQL database.

## Instalation
---
```
$ cd server
$ npm install
$ npm start
```

## How to use the API
---
API request must include both `api` and the API version. The API version is defined in `server/app.js`. For example, the root of the v1 API is at `/api/v1`.

The API uses JSON to serialize data. You don't need to specify `.json` at the end of the API URL.

# API LIST

| methods | Endpoint     | Description      |
| ------- | ------------ | --------------   |
| GET     | /todo        | lists of todos   |
| GET     | /todo/{id}   | view a todo      |
| POST    | /todo        | create new todo  |
| PUT     | /todo/{id}   | update a todo    |
| DELETE  | /todo/{id}   | delete a todo    |

---

| methods | Endpoint     | Description      |
| ------- | ------------ | --------------   |
| GET     | /users       | lists of users   |
| GET     | /user/{id}   | view a user      |
| POST    | /user        | create new user  |
| PUT     | /user/{id}   | update a user    |
| DELETE  | /user/{id}   | delete a user    |

---

| methods  | Endpoint     | Description              |
| -------  | ------------ | -------------------------|
| POST     | /signup      | Sign up a new user       |
| POST     | /login       | Sign in an existing user |

## Contributing
---
Pull requests are welcome. For major changes, please open an issue first to discuss what you like to change.

Please make sure to update test as appropriate.