const Sequelize = require('sequelize');
const db = require('../config/db');

const Todo = db.define(
	'tasks',
	{
		user_id: {
			type: Sequelize.DataTypes.INTEGER,
			foreignKey: true,
		},
		title: {
			type: Sequelize.DataTypes.STRING,
		},
		description: {
			type: Sequelize.DataTypes.STRING,
		},
		isComplete: {
			type: Sequelize.DataTypes.BOOLEAN,
		},
		createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
        },
          updatedAt: {
            allowNull: false,
            type: Sequelize.DATE,
        }, 
	},
	{
		classMethods: {
			associate: function(models) {
				Todo.belongsTo(models.User, {
					foreignKey: 'user_id'
				});
			}
		}
	},
	{
		timestamps: true,
	}
);

module.exports = Todo;