const Sequelize = require('sequelize');
const db = require('../config/db');
const validator = require('validator');

const User = db.define(
    'users', 
    {
        id: {
			type: Sequelize.DataTypes.INTEGER,
			primarykey: true,
		},
        email: {
            type: Sequelize.DataTypes.STRING,
            lowercase: true,
            validate: {
                isEmail: {
                    msg: 'Please provide a valid email'
                }
            },
            hooks: {
                beforeCreate: function(user) {
                    user.email = user.email.toLowerCase();

                    return user;
                }
            }
        },
        username: {
            type: Sequelize.DataTypes.STRING,
            unique: true,
            validate: {
                isAlphanumeric: {
                    msg: 'Input number and letter only'
                }
            }
        },
        name: {
            type: Sequelize.DataTypes.STRING
        },
        password: {
            type: Sequelize.DataTypes.STRING,
            validate: {
                min: 8,
                max: 23
            }
        }   
    },
    {
        timestamps: true,
    },
    {
        classMethods: {
            associate: function(models) {
                User.hasMany(models.Todo);
            }
        }
    },
);

module.exports = User;