const express = require('express');
const bp = require('body-parser');
const logger = require('morgan');
const cors = require('cors');

const app = express();

if (process.env.NODE_ENV === 'development') {
	app.use(logger('dev'));
}

app.use(cors());
app.use(bp.urlencoded({ extended: false }));
app.use(bp.json());

let todos = require('./routes/todosRoute');
let users = require('./routes/usersRoute');

app.use('/api/v1', todos);
app.use('/api/v1', users);

module.exports = app;