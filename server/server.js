require('dotenv').config({ path: './config.env'})
const app = require('./app')

console.log(app.get('env'));
// console.log(process.env);

const port = process.env.PORT || 8000
app.listen(port, (err) => {
	if (err) {
		console.log(err)
	}
	console.log('Running on port:', port)
})