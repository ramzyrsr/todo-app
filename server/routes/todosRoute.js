const express = require('express')

const router = express.Router()
const controller = require('../controllers/todosController')

router
    .route('/todo')
    .get(controller.getAllTodos)
    .post(controller.createTodo);

router
    .route('/todo/:id')
    .get(controller.getTodo)
    .put(controller.updateTodo)
    .delete(controller.deleteTodo);

module.exports = router
