const express = require('express');

const router = express.Router();
const controller = require('../controllers/userController');
const auth = require('../controllers/authUser');

router.post('/signup', auth.signup);
router.post('/login', auth.login);

router.get('/users', controller.getAllUsers);
router.post('/user', controller.createUser);
router
    .route('/user/:id')
    .get(controller.getUser)
    .put(controller.updateUser)
    .delete(controller.deleteUser);

module.exports = router
