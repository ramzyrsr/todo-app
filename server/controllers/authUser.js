const jwt = require('jsonwebtoken');
const passwordHash = require('password-hash');
const User = require('../models/usersModel');

const signToken = id => {
    return jwt.sign({ id }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN
    });
}

exports.signup = async (req, res, next) => {
    try {
        const user = await User.findOne({ where: {
            email: req.body.email
        }});

        if (user) {
            res
				.json({
					statusCode: 204,
					statusText: 'Success',
					message: 'Email has already registered'
				})
				.status(204);
        } else {
            const hashedPassword = passwordHash.generate(req.body.password);

            const newUser = await User.create({
                name: req.body.name,
                email: req.body.email,
                username: req.body.username,
                password: hashedPassword
            });

            const token = signToken(newUser.id);
        
            res.status(201).json({
                status: 'success',
                data: {
                    token: token,
                    user: newUser
                }
            })
        }
    } catch (error) {
        res
            .json({
                statusCode: 400,
                statusText: 'Error',
                message: error
            })
            .status(400);
    }
}

exports.login = async (req, res) => {
    try {
        const { email, password } = req.body;
        const user = await User.findOne({ where: { email } });
        const hashedPassword = user.dataValues.password;
        const verify = await passwordHash.verify(password, hashedPassword);

        const id = user.dataValues.id;
        const name = user.dataValues.name;
        const username = user.dataValues.username;

        if (email && password) {
            if (!user || !verify) {
                res
                    .json({
                        statusCode: 401,
                        statusText: 'Incorrect email or password',
                        message: error
                    })
                    .status(401);

                } else {
                    res
                        .json({
                            statusCode: 200,
                            statusText: 'success',
                            data: {
                                id,
                                name,
                                email,
                                username,
                                token: signToken(user.id)
                            }
                        })
                        .status(200);
                }
        }
        
        if (!email || !password) {
            res
                .json({
                    statusCode: 400,
                    statusText: 'Please provide email or password',
                    message: error
                })
                .status(400);
        }

    } catch (error) {
        resusername
            .json({
                statusCode: 400,
                statusText: 'Error',
                message: error
            })
            .status(400);
    }
}