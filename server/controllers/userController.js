const User = require('../models/usersModel');
const { timeStamp } = require('console');
const passwordHash = require('password-hash');
const methods = {};

methods.getUser = async (req, res) => {
	try {
		const user = await User.findOne({
			where: {
				id: req.params.id
			}
		})

		const resPayload = {
			statusCode: 200,
			statusText: 'Success',
			message: 'Get user',
			data: user,
		};
		res
			.json(resPayload)
			.status(200);

	} catch (error) {
		res
			.json(error)
			.status(400);
	}
};

methods.getAllUsers = async (req, res) => {
	try {
		const users = await User.findAll();
		const resPayload = {
			statusCode: 200,
			statusText: 'Success',
			message: 'Get all users',
			data: users,
		};
		res
            .json(resPayload)
            .status(200);

	} catch (error) {
		res
            .json(error)
			.status(400);
	}
};

methods.createUser = async (req, res) => {
	try {
		const user = await User.findOne({ where: {
			email: req.body.email
		} });
		if (user) {
			res
				.json({
					statusCode: 204,
					statusText: 'Success',
					message: 'Email has already registered'
				})
				.status(204);
		} else {
			const reqPayload = {
				email: req.body.email,
				username: req.body.username,
				name: req.body.name,
				password: passwordHash.generate(req.body.password),
				timeStamp
			};
	
			const newUser = User.create(reqPayload);
			if (newUser) {
				res
					.json({
						statusCode: 201,
						statusText: 'Created',
						message: 'User is created',
						data: reqPayload
					})
					.status(201);
			}
		}
	} catch (error) {
		res
            .json({
                statusCode: 400,
                statusText: 'Error',
                message: error
            })
            .status(400);
	}
};

methods.updateUser = async (req, res) => {
	try {
		await User.update(req.body, {
			where: {
				id: req.params.id
			}
		})
		res
			.json({
				statusCode: 200,
				statusText: 'Success',
				message: 'User is updated',
			})
			.status(200);

	} catch (error) {
		rres
            .json({
                statusCode: 400,
                statusText: 'Error',
                message: error
            })
            .status(400);
	}
};

methods.deleteUser = async (req, res) => {
	try {
		await User.destroy({
			where: {
				id: req.params.id
			}
		})
		res
			.json({
				statusCode: 204,
				statusText: 'No Content',
				message: 'User is deleted',
			})
			.status(204);

	} catch (error) {
		res
            .json({
                statusCode: 400,
                statusText: 'Error',
                message: error
            })
            .status(400);
	}
}

module.exports = methods
