const Todo = require('../models/todosModel');
const { timeStamp } = require('console');
const methods = {};

methods.getTodo = async (req, res) => {
	try {
		const todo = await Todo.findOne({
			where: {
				id: req.params.id
			}
		})

		const resPayload = {
			statusCode: 200,
			statusText: 'Success',
			message: 'Get all todos',
			data: todo,
		};
		res
			.json(resPayload)
			.status(200);

	} catch (error) {
		res
			.json(error)
			.status(400);
	}
};

methods.getAllTodos = async (req, res) => {
	try {
		const todos = await Todo.findAll();
		const resPayload = {
			statusCode: 200,
			statusText: 'Success',
			message: 'Get all todos',
			data: todos,
		};
		res
			.json(resPayload)
			.status(200);

	} catch (error) {
		res
			.json(error)
			.status(400);
	}
};

methods.createTodo = (req, res) => {
	try {
		const reqPayload = {
			user_id: req.body.user_id,
			title: req.body.title,
			description: req.body.description,
			isComplete: false,
			timeStamp
		};

		const newTodo = Todo.create(reqPayload);
		if (newTodo) {
			res
				.json({
					statusCode: 201,
					statusText: 'Created',
					message: 'Create todo is success',
					data: reqPayload
				})
				.status(201);
		}
	} catch (error) {
		res.json({
			statusCode: 400,
			statusText: 'Bad Request',
			message: error
		})
		.status(400);
	}
};

methods.updateTodo = async (req, res) => {
	try {
		await Todo.update(req.body, {
			where: {
				id: req.params.id
			}
		})
		res
			.json({
				statusCode: 200,
				statusText: 'Success',
				message: 'Todo is updated',
			})
			.status(200);

	} catch (error) {
		res.json({
			statusCode: 400,
			statusText: 'Bad Request',
			message: error
		})
		.status(400);
	}
};

methods.deleteTodo = async (req, res) => {
	try {
		await Todo.destroy({
			where: {
				id: req.params.id
			}
		})
		res
			.json({
				statusCode: 204,
				statusText: 'No Content',
				message: 'Todo is deleted',
			})
			.status(204);

	} catch (error) {
		res.json({
			statusCode: 400,
			statusText: 'Bad Request',
			message: error
		})
		.status(400);
	}
}

module.exports = methods