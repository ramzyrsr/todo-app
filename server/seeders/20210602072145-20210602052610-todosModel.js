'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('tasks', [
      {
        user_id: '1',
        title: 'playing',
        description: 'vs code',
        isComplete: false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id: '2',
        title: 'running',
        description: 'postman',
        isComplete: false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id: '3',
        title: 'fetching',
        description: 'DBeaver',
        isComplete: false,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ])
   },
 
   down: async (queryInterface, Sequelize) => {
      await queryInterface.bulkDelete('tasks', null, {});
   }
};
